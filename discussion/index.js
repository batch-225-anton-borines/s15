console.log("Hello, World!")
// COMMENTS in JavaScript
// To wrtie comments, we use two forward slashes single line comments, and two forward and two asterisks for multi-line comments.
// This is a single line comment.
/*
	This is a multi-line comment.
	To write them, we add two asterisks inside of the forward slashes and write comments in between them.
*/

// VARIABLE
/*
	- Variables contain values that can be changed over the execution time of the program.
	- To declare a variable, we use the "let" keyword.

*/

let	productName = 'desktop computer'
console.log(productName);
productName = 'cellphone';
console.log(productName);

let productPrice = 500;
console.log(productPrice);
productPrice = 450;
console.log(productPrice);

// CONSTANTS
/*
	- Use constants for values that will not change.
*/

const deliveryFee = 30;
console.log(deliveryFee);

// DATA TYPES

// 1. STRINGS
/*
	-Strings are a series of characters that create a word, a phrase, sentence, or anything related to "TEXT".
	-Strings in JavaScript can be written using a single quotes ('') or a double quotes ("")
	-In other programming languages, only the double quotes can be used for creating strings.
*/

let country = 'Philippines';
let province = "Metro Manila"

// CONCATENATION
console.log(country + ", " + province);

// 2. NUMBERS
/*
	-Include integers/whole numbers, decimal numbers, fraction, exponential notation
*/
let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI);

console.log(Math.PI);

console.log("John's grade last quarter is: " + grade);

// 3. BOOLEAN
/*
	-Boolean values are logical values.
	-Boolean values can contain either "true" or "false"
*/

let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// 4. OBJECTS

	// Arrays
	/*
		-They are a special kind of data that stores multiple values.
		-They are a special type of object.
		-They can store different data types, but is normally used to store similar data types.
	*/

	// Syntax
		// let/const arrayName = [ElementA, ElementB,...ElementN]
	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);
	console.log(grades[1]);

	let userDetails = ["John", "Smith", 32, true];
	console.log(userDetails);

	// Object Literals
	/*
		-Objects are another special kind of data type that mimics real world objects/items
		-They are used to create complex data that contains pieces of information thatare relevant to each other.
		-Every individual piece of information is called a property of the object

		Syntax:

			let/const objectName: {
				propertyA: value,
				propertyB: value
			}
	*/

	let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ['+639876543210', '024758463'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails);

	// 5. NULL
	/*
		-It is used to intentionally express the absence of a value.
	*/

	let spouse = null;
	console.log(spouse);

	// 6. UNDEFINED
	/*
		- Represents that the state of a variable that has been declared but without an assigned value.
	*/
	let vacationPlace;
	console.log(vacationPlace);